// importing the "http" module
const http = require("http");


// storing the 4000 in a variable called port
const port = 4000;

// storing the createServer method inside the server variable 
const server = http.createServer(function(request, response){



	// HTTP method can be accessed via "method" property 
	if (request.url === "/items" && request.method==="GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retreived from the database");
	}


	// POST REQUEST
	// "POST" means that we will be adding/creating information 
	if(request.url==="/items"&& request.method==="POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data to be sent to the database");
	}

	



});

/*
	GET method
		-means that we will be retreiving or reading information
*/
/*
	POSTMAN
		-since the Node js application that we are building is a backend application, there is no frontend application yet to process the request

*/

server.listen(port);


// confirmation that the server is running
console.log(`Server now running at port: ${port}`);




// npx kill-port <port> - used to 